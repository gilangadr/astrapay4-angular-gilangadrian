export interface User{
    id: number,
    name: string,
    phone: number,
    username : string,
    website : string,
    email : string,
    company: {
      bs: string,
      catchPhrase: string,
      name: string,
    },
    address: {
      city : string,
      street : string,
      suite : string,
      zipcode : string,
      geo :{
        lat: string,
        lng: string,
      }
    }
  }

  export interface UserAP {
    id_user: number,
    username: string,
    email: string,
    nama_lengkap: string,
    tanggal_lahir: string,
    dompet: {
        id_dompet: number,
        saldo_astrapay: number,
        poin_astrapay : number,
    },
    role: {
        id_role: number,
        nama_role: string
    },
    photo?: string
  }

  export interface RegisterModal{
    username: string,
    password: string,
    email: string,
    nama_lengkap: string,
    tanggal_lahir: string,
    photo?: string
  }

  export interface ResponseUploadPhoto{
    image: string
  }