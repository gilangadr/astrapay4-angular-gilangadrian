export interface requestLogin{
    username: string,
    password: string,
}

export interface ResponseLogin{
    token: string,
    role: string
}