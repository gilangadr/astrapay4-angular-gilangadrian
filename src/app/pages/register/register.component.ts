import { Component } from '@angular/core';
import * as dayjs from 'dayjs';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
  username = '';
  password = '';
  email = '';
  nama_lengkap = '';
  tanggal_lahir = '';

  formRegister: FormGroup

  error = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required]], //, Validators.email
      password: ['', [Validators.required, Validators.minLength(6)]],
      c_password: ['', [Validators.required]] ,
      email: ['', [Validators.required]],
      nama_lengkap: ['', [Validators.required]],
      tanggal_lahir: ['', [Validators.required]],
    })
  }

  get errorControl() {
    return this.formRegister.controls;
  }

  get confirmPassword(){
    return this.formRegister.value.password == this.formRegister.value.c_password 
  }

  doRegister() {

    console.log(this.formRegister);
    
    const payload = {
      username: this.formRegister.value.username,
      password: this.formRegister.value.password,
      email: this.formRegister.value.email,
      nama_lengkap: this.formRegister.value.nama_lengkap,
      tanggal_lahir: dayjs(this.formRegister.value.tanggal_lahir).format('YYYY-MM-DD'),
    }

    this.authService.register(payload).subscribe(
      response => {
        console.log(response)
        this.router.navigate(['/login'])
      }, error => {
        console.log(error);
        alert(error.error.message)
      }
    )
  }
}
