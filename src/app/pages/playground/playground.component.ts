import { Component } from '@angular/core';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent {
  title = 'AngularIntro';

  name = 'Gilang'
  age = 26
  status = false

  showData= false

  nomor = 1

  person = { 
    title: 'Test A',
    name: 'Dut',
    age: 22,
    status: true
  }

  personList = [{
    title: 'Test B',
    name: 'Dut',
    age: 22,
    status: true
  }, 
  {
    title: 'Test C',
    name: 'Dut',
    age: 22,
    status: true
  },
  ]

  datas = [1,2,3,]
  

  constructor(){
    this.name = 'Adrian'
    this.age = 26
  }

  onCallBack(ev: any) {
    console.log(ev)
  }

  onCallBackAdd(ev: any) {
    ev.data.title = "ini tambahan aja sih"
    this.personList.push(ev.data)
    console.log(this.personList);
  }
}
