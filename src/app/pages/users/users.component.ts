import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {
  userList!: User[]

  constructor(private dataService: DataService) {
    this.dataService.getUser().subscribe(
      response => {
        this.userList = response
        console.log(response)
      }
    )
   }

}