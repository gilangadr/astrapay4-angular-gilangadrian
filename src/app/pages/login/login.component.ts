import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Role } from 'src/app/enums/role';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  username = '';
  password = '';

  formLogin: FormGroup

  // error = ''

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  get errorControl() {
    return this.formLogin.controls;
  }
  

  doLogin() {
    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }

    this.authService.login(payload).subscribe(
      response => {
        console.log(response)
        
        alert(response.data.role)
        if (response.data.role === Role.ADMIN) {
          localStorage.setItem('token', response.data.token)
          this.router.navigate(['/admin'])
          console.log("ok")
        }
        if (response.data.role === Role.USER) {
          this.router.navigate(['/playground'])
        } else {
          //tidak melakukan apaapa
        }
      }, error => {
        console.log(error);
        alert(error.error.message)
      }
    )
  }
}
