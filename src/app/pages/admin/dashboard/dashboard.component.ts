import { Component, OnInit, TemplateRef } from '@angular/core';
import { UserAP } from 'src/app/interfaces/user.interface';
import { UsersService } from 'src/app/services/users.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AuthService } from 'src/app/services/auth.service';
import { map, switchMap } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  modalRef?: BsModalRef;
  user: UserAP[] = []
  formAdd: FormGroup

  photo!: string
  photoFile!: File

  //refresh = new Subject<void>();

  constructor(
    private usersService: UsersService,
    private modalService: BsModalService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    this.usersService.getAPUsers().subscribe(
      response => {
        console.log(response)
        this.user = response
      }
    )
    this.formAdd = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      email: ['', [Validators.required, Validators.email]],
      nama_lengkap: ['', [Validators.required]],
      tanggal_lahir: ['', [Validators.required]],
      photo: ['', [Validators.required]],
    });
  }

  get errorControl(){
    return this.formAdd.controls
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  showPreview(event: any) {
    if (event) {
      const file = event.target.files[0]
      this.photoFile = file
      const reader = new FileReader();
      reader.onload = () => {
        this.photo = reader.result as string
      }
      reader.readAsDataURL(file);
    }
  }

  doAddUser() {
    this.usersService
      .uploadPhoto(this.photoFile)
      .pipe(
        switchMap(val => {
          const payload = {
            username: this.formAdd.value.username,
            password: this.formAdd.value.password,
            email: this.formAdd.value.email,
            nama_lengkap: this.formAdd.value.nama_lengkap,
            tanggal_lahir: this.formAdd.value.tanggal_lahir,
            photo: val.data
          }

          return this.authService.register(payload).pipe(
            map(val => val)
          )
        })
      ).subscribe(response => console.log(response))
  }

}
