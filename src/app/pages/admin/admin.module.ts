import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes } from '@angular/router';
import { LayoutsModule } from 'src/app/layouts/layouts.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { CategoryComponent } from './product/category/category.component';
import { ListProductComponent } from './product/list-product/list-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'category',
        component: CategoryComponent
      },
      {
        path: 'list-product',
        component: ListProductComponent
      }
    ]
  }
]

@NgModule({
  declarations: [AdminComponent, DashboardComponent, SettingsComponent, CategoryComponent, ListProductComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    LayoutsModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AdminModule { }
