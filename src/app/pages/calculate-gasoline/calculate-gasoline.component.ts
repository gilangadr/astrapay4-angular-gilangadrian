import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-calculate-gasoline',
  templateUrl: './calculate-gasoline.component.html',
  styleUrls: ['./calculate-gasoline.component.scss']
})
export class CalculateGasolineComponent {
  
  bensin = "?"
  harga = 0
  total = 0
  change = 0
  status = "(pembayaran belum diproses)"
  showChange = false

  constructor(private dataService: DataService){
    this.harga = this.dataService.baseHarga
  }

  testClass = "alert alert-success"

  @Input() jumlahLiter = 0
  @Input() jumlahBayar = 0

  @Output() dataCallBack = new EventEmitter()

  cekHarga(value: any) {
    if (value == 'Pertamax') {
      this.harga = 15000
      this.bensin = "Pertamax"

    } else if (value == 'Pertalite') {
      this.harga = 12000
      this.bensin = "Pertalite"

    } else if (value == 'Solar') {
      this.harga = 5000
      this.bensin = "Solar"

    } else if (value == 'PertamaxPlus') {
      this.harga = 17500
      this.bensin = "PertamaxPlus"

    } else {
      this.harga = 0
      this.bensin = "Bengsing tidak ditemukan"
    }
  }

  hitungHarga(liter: number, bayar: number) {

    if(this.bensin == "?"){
      this.status = "Pilih bengsing sebelum membayar"
    } else{
      this.total = this.harga * liter
      if (this.total > bayar) {
        this.status = "Uang anda tidak cukup"
      } else {
        this.status = "Pembelian bengsing anda berhasil!"
        this.change = bayar - this.total
        this.showChange = true
    }
    }

  }
}
