import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { ResponseUploadPhoto, User, UserAP } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  private baseApi = 'http://localhost:8080';
  private baseApiNgrok = "https://1cfe-125-164-20-251.ap.ngrok.io"

  constructor(private httpClient: HttpClient) { }

  getAPUsers(): Observable<UserAP[]> {
    // const token = localStorage.getItem('token');
    // const header = new HttpHeaders({
    //   Authorization: `Bearer ${token}`,
    // });
    return this.httpClient
      .get<ResponseBase<UserAP[]>>(`${this.baseApi}/usersget`) 
      //, {
        //headers: header,
      //}
      .pipe(
        map((val) => {
          const data: UserAP[]= []
          for(let item of val.data){
            data.push({
              id_user: item.id_user,
              username: item.username,
              email: item.email,
              nama_lengkap: item.nama_lengkap,
              tanggal_lahir: item.tanggal_lahir,
              dompet: {
                id_dompet : item.dompet.id_dompet,
                saldo_astrapay : item.dompet.saldo_astrapay,
                poin_astrapay : item.dompet.poin_astrapay
              },
              role:{
                id_role : item.role.id_role,
                nama_role : item.role.nama_role
              },
              photo: `${this.baseApi}/files/${item.photo}:.+`
            })
          }
          return data;
        }),
        catchError((err) => {
          console.log(err);
          throw err;
        })
      );
  }
  
  uploadPhoto(data: File): Observable<ResponseBase<string>> {
    const file = new FormData()
    file.append('file', data, data.name)
    return this.httpClient.post<ResponseBase<string>>(`${this.baseApi}/upload-file`, file)
  }

  addUser(payload: any) {
    return this.httpClient.post<ResponseBase<any>>(`${this.baseApi}/users/register`, payload)
  }
}
