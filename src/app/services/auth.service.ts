import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { requestLogin, ResponseLogin } from '../interfaces/auth.interface';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { Role } from '../enums/role';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseApi = 'http://localhost:8080'

  public isAdmin = new BehaviorSubject<boolean>(false)
  public isUser = new BehaviorSubject<boolean>(false)

  public isAuthenticated = new BehaviorSubject<boolean>(false)

  constructor(private httpClient: HttpClient) { 
    this.isLogin()
  }

  login(payload: requestLogin): Observable<ResponseBase<ResponseLogin>> {
    return this.httpClient.post<ResponseBase<ResponseLogin>>(this.baseApi + '/login', payload).pipe(
      tap(val => {
        console.log(val)
        if (val.data.role === Role.ADMIN) {
          this.isAdmin.next(true)
        } else if (val.data.role === Role.USER) {
          this.isUser.next(true)
        }
      })
    )
  }

  register(payload: { 
    username: string, 
    password: string, 
    email: string, 
    nama_lengkap: string, 
    tanggal_lahir: string }) {
    return this.httpClient.post(this.baseApi + '/register', payload)
  }

  private isLogin() {
    const token = localStorage.getItem('token')
    if(token) {
      this.isAdmin.next(true)
    }
  }
}
